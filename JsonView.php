<?php

require_once('View.php');
require_once('IJsonView.php');

class JsonView extends View implements IJsonView {

    public function outputJson($data) {
        header("Content-type: application/json");
        echo json_encode($data);
    }
}

?>
